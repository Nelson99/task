import logo from './logo.svg';
import './App.css';
import ContactList from './components/container/contact_list';
import EjercisioElement from './components/pure/EjercisioElement';

function App() {
  return (
    <div className="App">
      {/*<ContactList></ContactList>*/}
      <EjercisioElement></EjercisioElement>
    </div>
  );
}

export default App;
